<?php

namespace App\Entity;

use Symfony\Component\Validator\Constraints as Assert;

class PasswordUpdate
{
    private $ancienMotDePasse;

    /**
     * @Assert\Length(min=8, minMessage="Your password must be at least 8 characters long !")
     */
    private $nouveauMotDePasse;

    /**
     * @Assert\EqualTo(propertyPath="nouveauMotDePasse", message="You haven't correctly confirmed your new password !")
     */
    private $confirmMotDePasse;

    public function getAncienMotDePasse(): ?string
    {
        return $this->ancienMotDePasse;
    }

    public function setAncienMotDePasse(string $ancienMotDePasse): self
    {
        $this->ancienMotDePasse = $ancienMotDePasse;

        return $this;
    }

    public function getNouveauMotDePasse(): ?string
    {
        return $this->nouveauMotDePasse;
    }

    public function setNouveauMotDePasse(string $nouveauMotDePasse): self
    {
        $this->nouveauMotDePasse = $nouveauMotDePasse;

        return $this;
    }

    public function getConfirmMotDePasse(): ?string
    {
        return $this->confirmMotDePasse;
    }

    public function setConfirmMotDePasse(string $confirmMotDePasse): self
    {
        $this->confirmMotDePasse = $confirmMotDePasse;

        return $this;
    }
}