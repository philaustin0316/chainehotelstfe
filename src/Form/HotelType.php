<?php

namespace App\Form;

use App\Entity\Pays;
use App\Entity\Hotel;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class HotelType extends ApplicationType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom', TextType::class, $this->getConfiguration("Name", "Write the name..."))
            ->add('nombreChambres', IntegerType::class, $this->getConfiguration("Number of rooms", "Indicate the number..."))
            ->add('nombreEtoiles', IntegerType::class, $this->getConfiguration("Number of stars", "Indicate the number..."))
            ->add('description', TextareaType::class, $this->getConfiguration("Description", "Describe here..."))
            ->add('adresse', TextareaType::class, $this->getConfiguration("Address", "Fill in here..."))
            ->add('codePostal', TextType::class, $this->getConfiguration("Postal code", "Fill in here..."))
            ->add('ville',TextType::class, $this->getConfiguration("City", "Fill in here..."))
            ->add('telephone', TelType::class, $this->getConfiguration("Telephone", "Fill in here..."))
            ->add('prixCoefficient', MoneyType::class, $this->getConfiguration("Coefficient", "Fill in here..."))
            ->add('pays', EntityType::class, $this->getConfiguration("Country", "Choose your country...", [
                'class' => Pays::class,
                'choice_label' => 'nom'
            ]))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Hotel::class,
        ]);
    }
}