<?php

namespace App\Form;

use App\Entity\Pays;
use App\Entity\Utilisateur;
use App\Form\ApplicationType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class RegistrationType extends ApplicationType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom', TextType::class, $this->getConfiguration("Last name", "Your lastname..."))
            ->add('prenom', TextType::class, $this->getConfiguration("First name", "Your firstname..."))
            ->add('dateNaissance', BirthdayType::class, $this->getConfiguration("Date of birth", "Select a value..."))
            ->add('sexe', ChoiceType::class, $this->getConfiguration("Orientation", "Your orientation", [
                'choices' => [
                    'Homme' => 'H',
                    'Femme' => 'F'],
                'expanded' => true,
                'multiple' => false
            ]))
            ->add('adresse', TextareaType::class, $this->getConfiguration("Address", "Your address..."))
            ->add('codePostal', TextType::class, $this->getConfiguration("Postal code", "Your postal code..."))
            ->add('ville',TextType::class, $this->getConfiguration("City", "Your city..."))
            ->add('email', EmailType::class, $this->getConfiguration("Email address", "Your email address..."))
            ->add('motDePasse', PasswordType::class, $this->getConfiguration("Password", "Your password..."))
            ->add('confirm_motDePasse', PasswordType::class, $this->getConfiguration("Confirm password", "Confirm your password..."))
            ->add('pays', EntityType::class, $this->getConfiguration("Country", "Choose your country...", [
                'class' => Pays::class,
                'choice_label' => 'nom'
            ]))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Utilisateur::class,
        ]);
    }
}