<?php

namespace App\Controller;

use App\Entity\Carroussel;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="homepage")
     */
    public function index()
    {
        $repo = $this->getDoctrine()->getRepository(Carroussel::class);

        $tabCarroussel = $repo->findAll();

        return $this->render('home/index.html.twig', [
            'carroussel' => $tabCarroussel
        ]);
    }
}