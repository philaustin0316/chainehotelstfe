<?php

namespace App\Controller;

use App\Entity\Hotel;
use App\Form\HotelType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

class HotelController extends AbstractController
{
    /**
     * @Route("/hotels", name="hotellist")
     */
    public function index()
    {
        $repo = $this->getDoctrine()->getRepository(Hotel::class);

        $tabHotels = $repo->findAll();

        return $this->render('hotel/index.html.twig', [
            'hotels' => $tabHotels
        ]);
    }

    /**
     * Permet d'afficher le formulaire d'inscription
     * 
     * @Route("/addHotel", name="new_hotel")
     * 
     * @return Response
     */
    public function addNewHotel(Request $request, EntityManagerInterface $manager)
    {
        $hotel = new Hotel();

        $form = $this->createForm(HotelType::class, $hotel);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid())
        {
            $manager->persist($hotel);
            $manager->flush();

            $this->addFlash('success', "Your hotel is created with success ! You can now view in the list into the table!");

            return $this->redirectToRoute("hotellist");
        }

        return $this->render('hotel/new.html.twig', [
            'form' => $form->createView()
        ]);
    }
}